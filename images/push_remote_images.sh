#!/bin/bash

source ../settings.sh


IMAGENAME=$IMAGENAME_PYTHON

echo "Push remote images $DOCKER_REGISTRY/$IMAGENAME"
echo

docker image push --all-tags $DOCKER_REGISTRY/$IMAGENAME


IMAGENAME=$IMAGENAME_SELENIUM

echo "Push remote images $DOCKER_REGISTRY/$IMAGENAME"
echo

docker image push --all-tags $DOCKER_REGISTRY/$IMAGENAME
