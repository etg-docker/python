#!/bin/bash

source ../settings.sh

IMAGENAME=$IMAGENAME_PYTHON

echo "Build image $IMAGENAME"
echo

docker buildx build --progress=plain --tag $IMAGENAME:$DOCKER_TAG_CURRENT --build-arg PYTHON_VERSION=$PYTHON_VERSION . \
&& \
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_LATEST \
&& \
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_INTERNAL


IMAGENAME=$IMAGENAME_SELENIUM

echo "Build image $IMAGENAME"
echo

docker buildx build --progress=plain --file Dockerfile.Selenium --tag $IMAGENAME:$DOCKER_TAG_CURRENT --build-arg PYTHON_VERSION=$PYTHON_VERSION . \
&& \
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_LATEST \
&& \
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_INTERNAL
