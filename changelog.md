# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]


## [1.8.0-3.13.1] - 2025-02-08

### Added

- libraries: `pysvn`, `GitPython` #12

### Changed

- python: `3.13.1`

### Fixed

- selenium test with new version


## [1.7.0-3.12.4] - 2024-07-26

### Added

- libraries: `docxtpl`, per dependency `python_docx` and `jinja2` #9



## [1.6.0-3.12.4] - 2024-07-18

### Added

- libraries: `pypdf`, `cryptography` #8
- library `pyexcel-xlsx` for completion of the `pyexcel` libraries

### Changed

- python: `3.12.4`

### Fixed

- typo in library list of readme


## [1.5.0-3.12.2] - 2024-03-08

### Added

- libraries: `pyexcel`, `pyexcel-ods3` #7


## [1.4.0-3.12.2] - 2024-03-07

### Added

- library: `parameterized` #6


## [1.3.0-3.12.2] - 2024-02-15

### Added

- new docker image for selenium `ekleinod/python-selenium` #5
- library: `selenium` #5
- apk packages: `chromium`, `chromium-chromedriver`, `libffi-dev` #5

### Changed

- python: `3.12.2`


## [1.2.0-3.11.0] - 2023-03-22

### Added

- library: `python-gitlab` #4

### Changed

- python: `3.11.0`


## [1.1.0-3.10.0] - 2021-10-20

### Added

- library: `pytodotxt` #3
- library: `ruamel.yaml` #2

### Changed

- python: `3.10.0`


## [1.0.1-3.9.7] - 2021-09-11

### Changed

- calling image with user- and group id to avoid access permission issues
- python: `3.9.7`



## [1.0.0-3.9.2] - 2021-02-28

- initial version #1
- python: `3.9.2`

### Added

- docker image
- test files
- CI/CD for gitlab


[Unreleased]: https://gitlab.com/etg-docker/python/-/compare/1.8.0-3.13.1...HEAD
[1.8.0-3.13.1]: https://gitlab.com/etg-docker/python/-/compare/1.6.0-3.12.4...1.8.0-3.13.1
[1.7.0-3.12.4]: https://gitlab.com/etg-docker/python/-/compare/1.6.0-3.12.4...1.7.0-3.12.4
[1.6.0-3.12.4]: https://gitlab.com/etg-docker/python/-/compare/1.5.0-3.12.2...1.6.0-3.12.4
[1.5.0-3.12.2]: https://gitlab.com/etg-docker/python/-/compare/1.4.0-3.12.2...1.5.0-3.12.2
[1.4.0-3.12.2]: https://gitlab.com/etg-docker/python/-/compare/1.3.0-3.12.2...1.4.0-3.12.2
[1.3.0-3.12.2]: https://gitlab.com/etg-docker/python/-/compare/1.2.0-3.11.0...1.3.0-3.12.2
[1.2.0-3.11.0]: https://gitlab.com/etg-docker/python/-/compare/1.1.0-3.10.0...1.2.0-3.11.0
[1.1.0-3.10.0]: https://gitlab.com/etg-docker/python/-/compare/1.0.1-3.9.7...1.1.0-3.10.0
[1.0.1-3.9.7]: https://gitlab.com/etg-docker/python/-/compare/1.0.0-3.9.2...1.0.1-3.9.7
[1.0.0-3.9.2]: https://gitlab.com/etg-docker/python/-/tags/1.0.0-3.9.2

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
