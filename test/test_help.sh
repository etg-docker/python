source ../settings.sh

echo "Empty call - shows help with $IMAGENAME_PYTHON."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_PYTHON \
	$IMAGENAME_PYTHON


echo
echo "Empty call - shows help with $IMAGENAME_SELENIUM."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_SELENIUM \
	$IMAGENAME_SELENIUM
