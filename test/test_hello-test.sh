source ../settings.sh

echo "Running hello test script."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_SELENIUM \
	--volume "${PWD}":/usr/src/myapp \
	$IMAGENAME_SELENIUM \
		scripts/hello-test.py
