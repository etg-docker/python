import unittest

from selenium import webdriver

class TestCorrect(unittest.TestCase):

	def setUp(self):

		options = webdriver.FirefoxOptions()
		options.add_argument("-headless")

		self.browser = webdriver.Firefox(options=options)
		self.vars = {}

	def tearDown(self):

		self.browser.close()
		self.browser.quit()

	def test_correct(self):

		self.browser.get("https://www.edgesoft.de/")
		self.browser.set_window_size(1920, 1000)

		self.assertEqual(self.browser.title, "edge-soft | edgesoft.de")


if __name__ == "__main__":
		unittest.main()
