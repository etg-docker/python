from datetime import datetime, timedelta

if __name__ == "__main__":

    print("Hello World!")
    print("Today is {} at {}.".format(datetime.now().date().isoformat(), datetime.now().time().isoformat(timespec='seconds')))
    print("Tomorrow is {}.".format((datetime.now() + timedelta(days = 1)).date().isoformat()))
