source ../settings.sh

echo "Shows version with $IMAGENAME_PYTHON."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_PYTHON \
	$IMAGENAME_PYTHON \
		--version


echo
echo "Shows version with $IMAGENAME_SELENIUM."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_SELENIUM \
	$IMAGENAME_SELENIUM \
		--version
