source ../settings.sh

echo "Running hello world script with $IMAGENAME_PYTHON."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_PYTHON \
	--user "$(id -u):$(id -g)" \
	--volume "${PWD}":/usr/src/myapp \
	$IMAGENAME_PYTHON \
		scripts/hello-world.py


echo
echo "Running hello world script with $IMAGENAME_SELENIUM."
echo

docker run \
	--rm \
	--name $CONTAINERNAME_SELENIUM \
	--user "$(id -u):$(id -g)" \
	--volume "${PWD}":/usr/src/myapp \
	$IMAGENAME_SELENIUM \
		scripts/hello-world.py
