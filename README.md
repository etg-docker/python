# etg-docker: ekleinod/python, ekleinod/python-selenium

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/python/)
- **Docker hub:** [ekleinod/python](https://hub.docker.com/r/ekleinod/python),  [ekleinod/python-selenium](https://hub.docker.com/r/ekleinod/python-selenium)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/python/), [issue tracker](https://gitlab.com/etg-docker/python/-/issues)


## Supported tags

- `1.8.0`, `1.8.0-3.13.1`, `latest`
- `1.7.0`, `1.7.0-3.12.4`
- `1.6.0`, `1.6.0-3.12.4`
- `1.5.0`, `1.5.0-3.12.2`
- `1.4.0`, `1.4.0-3.12.2`
- `1.3.0`, `1.3.0-3.12.2`
- `1.2.0`, `1.2.0-3.11.0`
- `1.1.0`, `1.1.0-3.10.0`
- `1.0.1`, `1.0.1-3.9.7`
- `1.0.0`, `1.0.0-3.9.2`


## What are these images?

These are docker images for the current python (python 3) and a python for selenium tests.
The images have the names `ekleinod/python` and `ekleinod/python-selenium`.

The image `ekleinod/python` is based on the official [python-alpine](https://hub.docker.com/_/python) image.

The image `ekleinod/python-selenium` is based on `ekleinod/python`.

The images are mainly for my projects, thus the dependencies of these projects are added to python via pip.
This approach is easier than customizing several python images.

The additional libraries of `ekleinod/python` are:

- convertdate
- cryptography
- [docxtpl](https://github.com/elapouya/python-docx-template)
	- [python_docx](https://github.com/python-openxml/python-docx) (dependency)
	- [jinja2](https://github.com/pallets/jinja/) (dependency)
- [GitPython](https://github.com/gitpython-developers/GitPython)
- icalendar
- parameterized
- [pypdf](https://github.com/py-pdf/pypdf)
- [pyexcel](https://github.com/pyexcel/pyexcel)
- [pyexcel-ods3](https://github.com/pyexcel/pyexcel-ods3)
- [pyexcel-xlsx](https://github.com/pyexcel/pyexcel-xlsx)
- [pysvn](https://sourceforge.net/projects/pysvn/)
- pytodotxt
- python-gitlab
- recurring-ical-events
- ruamel.yaml
- urllib3
- [xmltodict](https://github.com/martinblech/xmltodict)


The additional libraries of `ekleinod/python-selenium` are:

- [selenium](https://pypi.org/project/selenium/)


For the complete changelog, see [changelog.md](https://gitlab.com/etg-docker/python/-/blob/main/changelog.md)


## How to use the images

You can use the images as follows:

~~~ bash
docker run --rm --interactive --tty --name <containername> --user "$(id -u):$(id -g)" --volume "${PWD}":/usr/src/myapp ekleinod/python <python script>
docker run --rm --interactive --tty --name <containername> --user "$(id -u):$(id -g)" --volume "${PWD}":/usr/src/myapp ekleinod/python-selenium <python script>
~~~

Please note, that `/usr/src/myapp` is the work directory of the image.

For a simple test showing the python help page, run

~~~ bash
$ docker run --rm --name python ekleinod/python
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_hello-world.sh
$ ./test_version.sh
~~~


## Releases

The latest release will always be available with:

- `ekleinod/python:latest`
- `ekleinod/python-selenium:latest`

There are two naming schemes:

1. `ekleinod/python:<internal>-<python>`

	Example: `ekleinod/python:1.0.0-3.9.2`

2. internal version number `ekleinod/python:<major>.<minor>.<patch>`

	Example: `ekleinod/python:1.0.0`


## Build locally

In order to build the images locally, clone the repository and call

~~~ bash
$ cd images
$ ./build_images.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2021-2025 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
